
-- SUMMARY --

The Lucene API Node module allows you to remove individual nodes from the Lucene API search index.  

To submit bug reports and feature suggestions, or to track changes:

  http://drupal.org/project/issues/luceneapi_node_filter


-- REQUIREMENTS --

Lucene API 6.x-1

This has not yet been tested with the 6.x-2 branch of Lucene API.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

A new checkbox will be added to node forms: 'Exclude from search'.

If selected the node will be immediately removed from the search index.

If deselected, the node will be added back into the search when the site is indexed again (eg: via cron).

If a node type is hidden from the search (which can be set in the lucene api node module's admin settings), the checkbox will be disabled.

-- CONTACT --

Current maintainers:
* Ben Scott (ben.scott) - http://drupal.org/user/149339
  Brighton Digital Ltd.

This project has been sponsored by:
* Comic Relief
